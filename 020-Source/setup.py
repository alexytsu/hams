from setuptools import setup

setup(
    name="hams",
    version='0.1',
    packages=["hams"],
    include_package_data=True,
    install_requires=[
        'flask',
        'wtforms',
        'dominate',
        'visitor',
        'flask-login',
        'flask-bootstrap',
        'sqlalchemy',
        'flask-sqlalchemy',
        'flask-dotenv',
        'flask-wtf',
        'flask-login',
        'python-dotenv',
        'flake8',
        'autopep8'
    ],
)
