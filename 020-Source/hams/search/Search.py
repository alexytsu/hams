from hams.users.UserManager import UserManager
from hams.centres.CentreManager import CentreManager


class SearchEngine():

    def __init__(self):
        usermgr = UserManager()
        ctrmgr = CentreManager()
        self.providers = usermgr.get_Provider()
        self.centres = ctrmgr.get_all_centres()

    def get_matches(self, searchterm):
        self.providers = [x for x in self.providers if searchterm in x.username.lower(
        ) or searchterm in x.firstname.lower() or searchterm in x.lastname.lower()]

        self.providers = [x for x in self.providers if x.providerNO]

        self.centres = [x for x in self.centres if searchterm in x.centrename.lower(
        ) or searchterm in x.suburb.lower()]

        return self.providers, self.centres
