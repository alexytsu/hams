from hams import Database
from hams.bookings.booking import Booking
from hams.bookings.BookingManager import BookingManager


class TestMakeBooking():

    def test_booking_full(self):
        print("Testing booking in full")
        patient = "jack@gmail.com"
        centre = "Prince of Wales Hospital"
        provider = "sid@gmail.com"
        date = "2018-04-12"
        time = "13:30"
        msg = "I have a stomach ache."
        notes = None
        booking = Booking(patient, provider, centre, date, time, msg, notes)
        BookingManager.addBooking(booking)
        bookingObj = BookingManager.getBookings(patient).first()

        assert bookingObj.userId == patient
        assert bookingObj.centreId == centre
        assert bookingObj.providerId == provider
        assert bookingObj.date == date
        assert bookingObj.time == time
        assert bookingObj.msg == msg
        assert bookingObj.notes == notes

    def test_booking_no_msg(self):
        print("Testing booking with no message")
        patient = "isaac@gmail.com"
        centre = "UTS Health Service"
        provider = "toby@gmail.com"
        date = "2018-04-12"
        time = "14:30"
        msg = None
        notes = None

        booking = Booking(patient, provider, centre, date, time, msg, notes)
        BookingManager.addBooking(booking)
        bookingObj = BookingManager.getBookings(patient).first()

        assert bookingObj.userId == patient
        assert bookingObj.centreId == centre
        assert bookingObj.providerId == provider
        assert bookingObj.date == date
        assert bookingObj.time == time
        assert bookingObj.msg == msg
        assert bookingObj.notes == notes

    def test_multiple_booking(self):
        print("Testing multiple bookings with same provider and patient")
        patient = "tom@gmail.com"
        centre = "Prince of Wales Hospital"
        provider = "michael@gmail.com"
        date = "2018-04-12"
        time = "18:00"
        msg = "I have a stomach ache."
        notes = None
        booking = Booking(patient, provider, centre, date, time, msg, notes)
        BookingManager.addBooking(booking)

        print("Testing booking with no message")
        patient = "tom@gmail.com"
        centre = "UNSW Health Service"
        provider = "michael@gmail.com"
        date = "2018-04-12"
        time = "17:30"
        msg = None
        notes = None

        booking = Booking(patient, provider, centre, date, time, msg, notes)
        BookingManager.addBooking(booking)

        bookingList = list(BookingManager.getProviderBookings(provider))
        assert len(bookingList) == 2

        bookingList2 = list(BookingManager.getBookings(patient))
        assert len(bookingList2) == 2
