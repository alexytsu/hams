from hams import Database
from hams.prescriptions.Prescription import Prescription
from hams.prescriptions.PrescriptionManager import PrescriptionManager
from hams.bookings.booking import Booking
from hams.bookings.BookingManager import BookingManager
from hams import isFlash


class TestMakePrescription():

    def makeBooking(self, patient):
        centre = "Prince of Wales Hospital"
        provider = "toby@gmail.com"
        date = "2018-04-12"
        time = "18:00"
        msg = "I have a stomach ache."
        notes = None
        booking = Booking(patient, provider, centre, date, time, msg, notes)
        BookingManager.addBooking(booking)
        return booking

    def test_make_prescription_with_message(self):
        print("Testing making prescription with message")
        patient = "isaac@gmail.com"
        booking = self.makeBooking(patient)
        bookingId = booking.id
        name = "Panadol"
        dosage = 2
        frequency = 2
        days = 5
        info = "Take after eating."
        presObj = Prescription(bookingId, name, dosage, frequency, days, info)

        PrescriptionManager.store_prescription(presObj)
        getPres = Prescription.query.filter_by(bookingId=bookingId).first()

        assert presObj.bookingId == getPres.bookingId
        assert presObj.name == getPres.name
        assert presObj.dosage == getPres.dosage
        assert presObj.frequency == getPres.frequency
        assert presObj.days == getPres.days
        assert presObj.info == getPres.info

    def test_make_prescription_without_message(self):
        print("Testing making prescription without message")
        patient = "tom@gmail.com"
        booking = self.makeBooking(patient)
        bookingId = booking.id
        name = "Neurofen"
        dosage = 1
        frequency = 3
        days = 2
        info = None
        presObj = Prescription(bookingId, name, dosage, frequency, days, info)

        PrescriptionManager.store_prescription(presObj)
        getPres = Prescription.query.filter_by(bookingId=bookingId).first()

        assert presObj.bookingId == getPres.bookingId
        assert presObj.name == getPres.name
        assert presObj.dosage == getPres.dosage
        assert presObj.frequency == getPres.frequency
        assert presObj.days == getPres.days
        assert presObj.info == getPres.info

    def test_get_prescription_list(self):
        print("Testing multiple prescriptions per session")
        patient = "jack@gmail.com"
        booking = self.makeBooking(patient)
        bookingId = booking.id
        name = "Neurofen"
        dosage = 3
        frequency = 2
        days = 1
        info = None
        presObj = Prescription(bookingId, name, dosage, frequency, days, info)
        PrescriptionManager.store_prescription(presObj)

        name = "Panadol"
        dosage = 3
        frequency = 2
        days = 5
        info = "INFO"
        presObj2 = Prescription(bookingId, name, dosage, frequency, days, info)
        PrescriptionManager.store_prescription(presObj)

        presList = PrescriptionManager.get_prescription(patient)
        currPres = [presObj, presObj2]

        for x in range(0, 1):
            assert presList[x].bookingId == currPres[x].bookingId
            assert presList[x].name == currPres[x].name
            assert presList[x].dosage == currPres[x].dosage
            assert presList[x].frequency == currPres[x].frequency
            assert presList[x].days == currPres[x].days
            assert presList[x].info == currPres[x].info
