from hams import Database
from hams.bookings.booking import Booking
from hams.bookings.BookingManager import BookingManager


class TestAddNotes():

    def makeBooking(self, patient):
        centre = "UTS Health Service"
        provider = "toby@gmail.com"
        date = "2018-04-12"
        time = "18:00"
        msg = "I have a stomach ache."
        notes = None
        booking = Booking(patient, provider, centre, date, time, msg, notes)
        BookingManager.addBooking(booking)
        return booking

    def test_add_empty(self):
        print("Testing first input of notes to session")
        patient = "isaac@gmail.com"
        booking = self.makeBooking(patient)
        notes = "This patient has a mental disability."
        booking.editNotes(notes)
        bookingId = booking.id
        Database.session.commit()
        bookingObj = BookingManager.getBookingbyId(bookingId)

        assert bookingObj.userId == patient
        assert bookingObj.centreId == "UTS Health Service"
        assert bookingObj.providerId == "toby@gmail.com"
        assert bookingObj.date == "2018-04-12"
        assert bookingObj.time == "18:00"
        assert bookingObj.msg == "I have a stomach ache."
        assert bookingObj.notes == notes

    def test_add_empty(self):
        print("Testing appending of notes")
        patient = "isaac@gmail.com"
        booking = self.makeBooking(patient)
        notes = "This patient has a mental disability."
        booking.editNotes(notes)
        bookingId = booking.id
        Database.session.commit()
        bookingObj = BookingManager.getBookingbyId(bookingId)

        notes2 = "This patient has a mental disability. This patient must see a specialist immediately."
        bookingObj.editNotes(notes2)
        Database.session.commit()
        bookingObj = BookingManager.getBookingbyId(bookingId)

        assert bookingObj.userId == patient
        assert bookingObj.centreId == "UTS Health Service"
        assert bookingObj.providerId == "toby@gmail.com"
        assert bookingObj.date == "2018-04-12"
        assert bookingObj.time == "18:00"
        assert bookingObj.msg == "I have a stomach ache."
        assert bookingObj.notes == notes2
