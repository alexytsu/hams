from hams import Database
from hams.users.UserManager import UserManager


class TestMakeReferral():

    def test_make_referral(Self):
        print("Testing adding specialist")
        patient = "jack@gmail.com"
        specialist = "mike@gmail.com"
        user_manager = UserManager()
        user_manager.Change_Patient_Referal(patient, specialist)
        patientObj = user_manager.Get_Patient(patient)

        assert patientObj.referal == specialist
