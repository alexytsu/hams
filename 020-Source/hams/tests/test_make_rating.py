from hams import Database
from hams.rating.RatingsManager import RatingsManager
from hams.users.UserManager import UserManager
from hams.centres.HealthcareCentre import Centre
from hams.rating.Rating import Rating
from hams.provider.Provider import Provider


class TestMakeRating():

    def test_make_rating(self):
        print("Testing making a rating object")
        user = "jack@gmail.com"
        rating = "3"
        provider = None
        centre = "Royal Prince Alfred Hospital"
        rating_manager = RatingsManager()
        rating_manager.insert_rating(user, rating, provider, centre)
        ratingObj = Rating.query.filter_by(
            userId=user, centreId=centre).first()

        assert ratingObj.userId == user
        assert ratingObj.rating == 3
        assert ratingObj.providerId == provider
        assert ratingObj.centreId == centre

    def test_update_rating(self):
        print("Testing updating rating")
        user = "jack@gmail.com"
        rating = "3"
        provider = "sid@gmail.com"
        centre = None
        rating_manager = RatingsManager()
        rating_manager.insert_rating(user, rating, provider, centre)
        ratingObj = Rating.query.filter_by(
            userId=user, providerId=provider).first()

        assert ratingObj.userId == user
        assert ratingObj.rating == 3
        assert ratingObj.providerId == provider
        assert ratingObj.centreId == centre

        user = "hao@gmail.com"
        rating = "1"
        provider = "sid@gmail.com"
        centre = None
        rating_manager.insert_rating(user, rating, provider, centre)
        ratingObj2 = Rating.query.filter_by(
            userId=user, providerId=provider).first()

        assert ratingObj2.userId == user
        assert ratingObj2.rating == 1
        assert ratingObj2.providerId == provider
        assert ratingObj2.centreId == centre

        #providers = list(UserManager.get_Provider(provider))
        # roviders[0].recalculate_rating()

        #assert providers[0].current_rating == 2

        providerObj = Provider.query.filter_by(username=provider).first()
        providerObj.recalculate_rating()

        assert providerObj.current_rating == 2

    def test_replace_rating(self):
        print("Testing rating same service twice")
        user = "isaac@gmail.com"
        rating = "5"
        provider = None
        centre = "USYD Health Service"
        rating_manager = RatingsManager()
        rating_manager.insert_rating(user, rating, provider, centre)
        ratingObj = Rating.query.filter_by(
            userId=user, centreId=centre).first()

        assert ratingObj.userId == user
        assert ratingObj.rating == 5
        assert ratingObj.providerId == provider
        assert ratingObj.centreId == centre

        user = "isaac@gmail.com"
        rating = "1"
        provider = None
        centre = "USYD Health Service"
        rating_manager.insert_rating(user, rating, provider, centre)
        ratingList = list(Rating.query.filter_by(userId=user, centreId=centre))
        centreObj = Centre.query.filter_by(centrename=centre).first()
        centreObj.recalculate_rating()

        assert len(ratingList) == 1
        assert centreObj.current_rating == 1
