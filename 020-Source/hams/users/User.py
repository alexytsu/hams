from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash
from hams import Database


# An implementation of a User class that is compatible with flask-login
class User(UserMixin, Database.Model):
    id = Database.Column(Database.Integer, primary_key=True)
    username = Database.Column(Database.String(
        120), unique=True, nullable=False)
    passwordhash = Database.Column(Database.String(200), nullable=False)
    firstname = Database.Column(Database.String(50))
    lastname = Database.Column(Database.String(50))
    phone = Database.Column(Database.String(10))

    def __repr__(self):
        return "<User {}>".format(self.username)

    def __init__(self, username, password, firstname, lastname, phone):
        self.username = username
        self.passwordhash = generate_password_hash(password)
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone

    def get_id(self):
        return str(self.id)

    def validate(self, password):
        return check_password_hash(self.passwordhash, password)

   
    
        
