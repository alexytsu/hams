import os
import csv
import re

from flask import flash
from sqlalchemy import exc

from hams import app
from hams import Database
from hams.users.User import User
from hams.patient.Patient import Patient
from hams.users.User import User
from hams.provider.Provider import Provider
from hams.patient.Patient import Patient
from hams.provider.ProviderCentreTable import ProviderCentreTable as PCT
from hams.specialist.Specialist import specialist

class UserManager():

    def import_csv(self):

        filepath = os.path.join(app.config['DATA'], "patient.csv")

        with open(filepath) as patientcsv:
            reader = csv.reader(patientcsv, delimiter=',')
            for row in reader:
                username = row[0]
                password = row[1]
                firstname = row[2]
                lastname = row[3]
                phone = row[4]
                medicare = row[5]

                try:
                    Database.session.add(
                        Patient(
                            username=username,
                            password=password,
                            firstname=firstname,
                            lastname=lastname,
                            phoneNO=phone,
                            medicareNO = medicare,
                            referal = "",
                        )
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
        flash(("Imported patients.csv into database.", "success"))

    def register_user_from_form(self, form):
        username = form["username"]
        password = form["password"]
        firstname = form["firstname"]
        lastname = form["lastname"]
        phone = form["phone"]

        if not re.match(r"[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.com*$", username):
            flash(
                ("Invalid username format. Usernames must be a valid email address.", "error"))
            return False

        try:
            Database.session.add(
                User(
                    username=username,
                    password=password,
                    firstname=firstname,
                    lastname=lastname,
                    phone=phone,
                )
            )
            Database.session.commit()
        except exc.IntegrityError:
            Database.session.rollback()
            flash(("That username already exists", "error"))
            return False

        flash(("Registered user {}".format(username), "success"))
        return True

    def get_user(self,user_name):
        user = User.query.filter_by(username=user_name).first()
        return user
    
    def get_user_type(self,username):
        user = Patient.query.filter_by(username=username).first()
        if (user.medicareNO is not None):
            return "Patient"
        user = Provider.query.filter_by(username=username).first()
        if (user.providerNO is not None):
            return "Provider"
        return None
        
    def get_user_by_ID(self,user_id):
        return User.query.get(int(user_id))

    def get_Provider(self,username = None):
        if username is None:
            return Provider.query.all()
        return Provider.query.filter_by(username=username).first()

    def get_Specialist(self,username = None):
        if username is None:
            return specialist.query.all()
        return specialist.query.filter_by(username=username).first()

    def get_Providers_by_Centre(self,CentreName, providerName=None):
        if(providerName is None):
            return PCT.query.filter_by(centre=CentreName)
        else:
            return PCT.query.filter_by(provider=providerName, centre=CentreName)

    def Change_Provider_Avaliability(self,providerName, timeslot,centre):
        provider = PCT.query.filter_by(provider=providerName,centre = centre).first()
        availability_list = list(provider.availability)
        availability_list[int(timeslot)] = 'n'
        provider.availability = "".join(availability_list)
        Database.session.commit()
    
    def Get_Patient(self,username):
        return Patient.query.filter_by(username=username).first()

    def Change_Patient_Referal(self,patientName, specialistName):
        patient = Patient.query.filter_by(username = patientName).first()
        patient.UpdateReferal(specialistName)
        print (specialistName)
        Database.session.commit()