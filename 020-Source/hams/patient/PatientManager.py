import csv
import os

from sqlalchemy import exc
from flask import flash

from hams import app, Database
from hams.patient.Patient import Patient
from hams.users.UserManager import UserManager

class PatientManager(UserManager):

    def import_csv(self):
        filepath = os.path.join(app.config['DATA'], "patient.csv")
        with open(filepath) as patientcsv:
            reader = csv.reader(patientcsv, delimiter=',')
            for row in reader:
                username = row[0]
                password = row[1]
                firstName = row[2]
                lastName = row[3]
                phoneNO = row[4]
                medicare = row[5]
                
                try:
                    Database.session.add(
                        Patient(
                            username=username,
                            password=password,
                            firstName=firstName,
                            lastName=lastName,
                            phoneNO=phoneNO,
                            medicareNO=medicare,
                            referal = "",
                        )
                    )
                except exc.IntegrityError:
                    Database.session.rollback()
                    flash("Could not add:", username)
