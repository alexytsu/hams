from hams import Database
from hams.users.User import User
from hams.bookings.booking import Booking


# patients are users except that they have medicare numbers
class Patient(User):
    medicareNO = Database.Column(Database.String(50))
    referal = Database.Column(Database.String(50))

    def __init__(self, username, password, firstname, lastname, phoneNO, medicareNO,referal):
        super().__init__(username, password, firstname, lastname, phoneNO)
        self.medicareNO = medicareNO
        self.referal = referal

    def getMedicare(self):
        return self.medicareNO

    def getViewNotes(self, provider):
        return [i.notes for i in Booking.query.filter((Booking.providerId != provider) & (Booking.userId == self.username))]

    def getEditNotes(self, provider):
        note = Booking.query.filter_by(
            userId=self.username, providerId=provider
        )[0]
        return note.notes

    def UpdateReferal(self,specialist):
        self.referal = specialist