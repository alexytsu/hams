import os
import csv

from sqlalchemy import exc
from flask import flash

from hams.history.History import History
from hams import app, Database


class HistoryManager():

    def import_csv(self):
        filepath = os.path.join(app.config['DATA'], "history.csv")

        with open(filepath) as historycsv:

            reader = csv.reader(historycsv, delimiter=',')
            for row in reader:
                patient = row[0]
                provider = row[1]
                notes = row[2]

                try:
                    Database.session.add(
                        Booking(
                            patientId = patient
                            providerId = provider
                            notes = notes
                        )
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
        flash(("Added history","success"))