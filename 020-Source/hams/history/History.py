from hams import Database

class History(Database.Model):
    id = Database.Column(Database.Integer, primary_key=True)
    patientId = Database.Column(Database.String(50))
    providerId = Database.Column(Database.String(50))
    notes = Database.Column(Database.String(50000))
    
    def __init__(self, patient, provider, notes)
        self.patientId = patient
        self.providerId = provider
        self.notes = notes

    def getNotes(self, patient, provider)
        return History.query.filter_by(providerId = provider, patientId = patient)