import os
import csv

from flask import flash
from sqlalchemy import exc

from hams import Database, app
from hams.centres.HealthcareCentre import Centre
from hams.provider.ProviderCentreTable import ProviderCentreTable as PCT


class CentreManager():

    def import_csv(self):

        filepath = os.path.join(app.config['DATA'], "health_centres.csv")

        with open(filepath) as centrecsv:
            reader = csv.reader(centrecsv, delimiter=',')
            for row in reader:
                type = row[0]
                code = row[1]
                name = row[2]
                name = name.strip()
                phone = row[3]
                suburb = row[4]
                try:
                    Database.session.add(
                        Centre(
                            code=code,
                            centrename=name,
                            centretype=type,
                            phone=phone,
                            suburb=suburb
                        )
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
                    flash(
                        ("{} is already registered into the database.".format(name), "warning"))

            flash(("Imported health_centres.csv into database.", "success"))

    def get_all_centres(self):
        return Centre.query.all()

    def get_centres_of_provider(self, providername):
        worked_centres = PCT.query.filter_by(provider=providername)
        centres = []
        for x in worked_centres:
            name = x.centre
            centre = Centre.query.filter_by(centrename=name).first()
            centres.append(centre)

        return centres

    def get_centre_code(self, centrename):
        return Centre.query.filter_by(centrename=centrename).first().code
