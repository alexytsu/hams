from hams import Database
from hams.rating.Rating import Rating


class Centre(Database.Model):
    id = Database.Column(Database.Integer, primary_key=True)
    code = Database.Column(Database.Integer)
    centrename = Database.Column(
        Database.String(50), unique=True, nullable=False)
    centretype = Database.Column(Database.String(10), nullable=False)
    phone = Database.Column(Database.String(10), nullable=False)
    suburb = Database.Column(Database.String(20), nullable=False)
    current_rating = Database.Column(Database.Integer)

    def __repr__(self):
        return "<Centre {}>".format(self.centrename)

    def __init__(self, code, centrename, centretype, phone, suburb):
        self.code = code
        self.centrename = centrename
        self.centretype = centretype
        self.phone = phone
        self.suburb = suburb

    # traverse the ratings database calculate a rating for the current centre
    def recalculate_rating(self):
        # calculate ratings
        ratings = Rating.query.filter_by(centreId=self.centrename)
        ratings = list(ratings)
        # calculate the average
        try:
            average_rating = sum([int(x.rating) for x in ratings])/len(ratings)
        except ZeroDivisionError:
            average_rating = "No Ratings Provided Yet"
        self.current_rating = average_rating
