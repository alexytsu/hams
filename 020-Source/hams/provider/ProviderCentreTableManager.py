import csv
import os

from sqlalchemy import exc
from flask import flash

from hams.provider.ProviderCentreTable import ProviderCentreTable as PCT
from hams import app, Database


class ProviderCentreTableManager():

    def import_csv(self):

        flash(("Imported pcts", "success"))

        filepath = os.path.join(
            app.config['DATA'], "provider_health_centre.csv")

        with open(filepath) as pctablecsv:
            reader = csv.reader(pctablecsv, delimiter=',')
            for row in reader:
                email = row[0]
                centre = row[1]
                start = int(row[2]) * 2
                end = int(row[3]) * 2
                availability = ""
                for i in range(0, 48):
                    if i >= start and i <= end:
                        availability += 'a'
                    else:
                        availability += 'n'

                copy = availability

                for i in range(0, 4):
                    availability += copy

                try:
                    Database.session.add(
                        PCT(provider=email, centre=centre,
                            availability=availability)
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
                    flash(("Could not add {}".format(email)))
    
    
