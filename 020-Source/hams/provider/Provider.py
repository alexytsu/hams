from hams import Database
from hams.users.User import User
from hams.rating.Rating import Rating


class Provider(User):
    providerNO = Database.Column(Database.String(10))
    profession = Database.Column(Database.String(50))
    current_rating = Database.Column(Database.Integer)

    def __init__(self, username, password, firstname, lastname, phone, providerNO, profession):
        super().__init__(username, password, firstname, lastname, "0433535687")
        self.providerNO = providerNO
        self.profession = profession

    def getProviderNum(self):
        return self.providerNO

    def getProfession(self):
        return self.profession

    def recalculate_rating(self):
        ratings = Rating.query.filter_by(providerId=self.username)
        ratings = list(ratings)
        try:
            average_rating = sum(x.rating for x in ratings) / len(ratings)
        except Exception:
            average_rating = "No ratings provided yet"
        self.current_rating = average_rating
