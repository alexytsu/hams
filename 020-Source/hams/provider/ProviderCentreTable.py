from hams import Database


class ProviderCentreTable(Database.Model):
    __tablename__ = 'pct'
    id = Database.Column(Database.Integer(), primary_key=True)
    provider = Database.Column(Database.String(50))
    centre = Database.Column(Database.String(50))
    availability = Database.Column(Database.String(48 * 5))

    def __init__(self, provider=None, centre=None, availability=None):
        self.provider = provider
        self.centre = centre
        self.availability = availability

    def getAvailability(self, provider, centre):
        return ProviderCentreTable.query.filter_by(provider=provider, centre=centre).first()

    def update(self, provider, centre, timeslot):
        Database.session.delete(ProviderCentreTable.query.filter_by(
            provider=provider, centre=centre).first())
        self.availability = self.availability[
            :timeslot - 1] + 'b' + self.availability[timeslot + 1:]
        Database.session.commit()

    def isAvailable(self, provider, centre, timeFromNow):
        availability = ProviderCentreTable.query.filter_by(
            provider=provider, centre=centre).first()
        return availability[timeFromNow]

    def getCentres(self, Provider):
        return [i for i in ProviderCentreTable.query.filter_by(provider=Provider)]

    def getProviders(self, Centre):
        return [i for i in ProviderCentreTable.query.filter_by(centre=Centre)]

    