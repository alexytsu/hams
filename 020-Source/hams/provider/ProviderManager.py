import os
import csv

from sqlalchemy import exc
from flask import flash

from hams.provider.Provider import Provider
from hams import app, Database
from hams.users.UserManager import UserManager


class ProviderManager(UserManager):

    def import_csv(self):

        filepath = os.path.join(app.config['DATA'], "provider.csv")

        with open(filepath) as providercsv:
            reader = csv.reader(providercsv, delimiter=',')
            for row in reader:
                username = row[0]
                password = row[1]
                profession = row[2]
                providerNo = row[3]
                providerLastname = row[4]
                print(providerLastname)
                try:
                    Database.session.add(
                        Provider(
                            username=username,
                            password=password,
                            firstname="Dr",
                            lastname=providerLastname,
                            phone='0433535687',
                            profession=profession,
                            providerNO=providerNo
                        )
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
                    flash(("Could not add {}".format(username), "error"))
        flash(("imported providers", "success"))
