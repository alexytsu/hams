import os
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

isFlash = 0
def create_app():
    app = Flask(__name__, instance_relative_config=True)

    # Define locations for folders etc.
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATA=os.path.join(app.root_path, 'data'),
        SQLALCHEMY_DATABASE_URI='sqlite:///database/database.db',
        SQLALCHEMY_TRACK_MODIFICATIONS=False
    )

    # Setup extra app features

    # Add Bootstrap framework for frontend
    Bootstrap(app)

    # Initialise the database
    db = SQLAlchemy(app)

    # Add flask-login's LoginManager
    login_manager = LoginManager()
    login_manager.init_app(app)
    return app, login_manager, db


app, login_manager, Database = create_app()

import hams.views
