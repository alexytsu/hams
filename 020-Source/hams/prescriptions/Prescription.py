from hams import Database
from flask import flash
from hams import isFlash


class Prescription(Database.Model):
    id = Database.Column(Database.Integer, primary_key=True)
    bookingId = Database.Column(Database.Integer)
    name = Database.Column(Database.String(50))  # name of drug
    dosage = Database.Column(Database.String(70))  # no. of tablets
    frequency = Database.Column(Database.String(70))  # how many times a day
    # how many days to take medication
    days = Database.Column(Database.Integer)
    info = Database.Column(Database.String(2000))  # extra info

    def __init__(self, booking=None, name=None, dosage=None, frequency=None, days=None, info=None):
        if (booking != '' and name != '' and dosage != '' and frequency != '' and days != ''):
            self.bookingId = int(booking)
            self.name = name
            self.dosage = dosage
            self.frequency = frequency
            self.info = info
            try:
                self.days = int(days)
                if isFlash:
                    flash(("Prescription added!", "success"))
            except Exception:
                if isFlash:
                    flash(("Failed to add prescription. Your input of '{}' days is invalid. Please input a number.".format(
                        days), "error"))
                raise TypeError
        else:
            if isFlash:
                flash(
                    ("Failed to add prescription. You must input a value in every box that is not labelled 'optional'.", "error"))

    def __str__(self):
        info = "Drug: {}\n".format(self.name)
        info += "Dosage: {}\n".format(self.dosage)
        info += "Frequency: {}\n".format(self.frequency)
        info += "Days: {}\n".format(self.days)
        info += "Additional Information: {}".format(self.info)
        info += "\n\n ********** \n\n"
        return info

    # return a list of prescriptions for specific patient

    def getName(self):
        return self.name

    def getDosage(self):
        return self.dosage

    def getFrequency(self):
        return self.frequency

    def getDays(self):
        return self.days

    def getInfo(self):
        return self.info

    def returnAllData(self):
        return[str(self.bookingId),
               self.name,
               str(self.dosage),
               str(self.frequency),
               str(self.days),
               self.info]
