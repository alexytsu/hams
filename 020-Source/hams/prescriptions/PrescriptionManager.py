import os
import csv

from sqlalchemy import exc
from flask import flash

from hams.prescriptions.Prescription import Prescription
from hams import app, Database
from hams.bookings.BookingManager import BookingManager as BM


class PrescriptionManager():

    def import_csv(self):
        filepath = os.path.join(app.config['DATA'], "presciption.csv")

        with open(filepath) as prescriptioncsv:

            reader = csv.reader(prescriptioncsv, delimiter=',')
            for row in reader:
                booking = row[0]
                name = row[1]
                dosage = row[2]
                frequency = row[3]
                days = row[4]
                info = row[5]
                try:
                    Database.session.add(
                        Prescription(
                            bookingId=booking,
                            name=name,
                            dosage=dosage,
                            frequency=frequency,
                            days=days,
                            info=info
                        )
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
        flash(("Added prescriptions", "success"))

    def get_prescription(patient):
        Prescriptions = []
        Bookings = list(BM.getBookings(patient))
        for booking in Bookings:
            Prescriptions += list(Prescription.query.filter_by(bookingId=booking.id))
            print(Prescriptions)
        return Prescriptions

    def store_prescription(prescription):
        try:
            Database.session.add(prescription)
            Database.session.commit()
        except exc.IntegrityError:
            Database.session.rollback()
            flash(("Prescription fields were invalid", "error"))
