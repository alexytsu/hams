import os
import csv 
from hams import Database
from hams.provider.Provider import Provider

class specialist(Provider):
    expertise = Database.Column(Database.String(50))
    def __init__(self,username,password,firstname,lastname,phone,providerNO,expertise):
        super().__init__(username, password,firstname,lastname,phone,providerNO,"Specialist")
        self.expertise = expertise

    def getExpertise(self):
        return self.expertise
