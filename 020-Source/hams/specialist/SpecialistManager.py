import os 
import csv 

from sqlalchemy import exc
from flask import flash
from hams import app, Database
from hams.specialist.Specialist import specialist

class SpecialistManager():
    def import_csv(self):

        filepath = os.path.join(app.config['DATA'],"specialist.csv")

        with open(filepath) as specialistcsv: 
            reader = csv.reader(specialistcsv, delimiter=',')
            for row in reader:
                username = row[0]
                password = row[1]
                expertise = row[2]
                providerNO = row[3]
                providerLastname = row[4]
                try:
                    Database.session.add(
                        specialist(
                            username=username,
                            password=password,
                            expertise = expertise,
                            phone = "0433429032",
                            firstname = "Dr",
                            lastname = providerLastname,
                            providerNO = providerNO
                        )
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
                    flash(("Could not add {}".format(username),"error"))
        flash(("imported Specialist","success"))

