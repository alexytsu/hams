import os
import csv

from sqlalchemy import exc
from flask import flash

from hams.bookings.booking import Booking
from hams import app, Database


class BookingManager():

    def import_csv(self):
        filepath = os.path.join(app.config['DATA'], "booking.csv")

        with open(filepath) as providercsv:

            reader = csv.reader(providercsv, delimiter=',')
            for row in reader:
                patient = row[0]
                provider = row[1]
                centrename = row[2]
                print(centrename)
                date = row[3]
                time = row[4]
                try:
                    Database.session.add(
                        Booking(
                            userId=patient,
                            providerId=provider,
                            centreId=centrename,
                            date=date,
                            time=time
                        )
                    )
                    Database.session.commit()
                except exc.IntegrityError:
                    Database.session.rollback()
        flash(("Added bookings", "success"))

    def getBookings(username=None):
        if(username is None):
            return Booking.query.all()
        else:
            return Booking.query.filter_by(userId=username)

    def getPatient(bookingID):
        booking = Booking.query.filter_by(id=bookingID).first()
        return booking.userId

    def getProviderBookings(username=None):
        if(username is None):
            return Booking.query.all()
        else:
            return Booking.query.filter_by(providerId=username)

    def getBookingbyId(id):
        return Booking.query.filter_by(id=id).first()

    def addBooking(booking):
        Database.session.add(booking)
        Database.session.commit()
