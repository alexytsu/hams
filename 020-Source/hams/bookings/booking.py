from hams import Database
from hams import isFlash


class Booking(Database.Model):
    id = Database.Column(Database.Integer, primary_key=True)
    userId = Database.Column(Database.String(50))
    providerId = Database.Column(Database.String(50))
    centreId = Database.Column(Database.String(50))
    date = Database.Column(Database.String(50))
    time = Database.Column(Database.String(30))
    msg = Database.Column(Database.String(50000))
    notes = Database.Column(Database.String(10000000))

    def __repr__(self):
        return "<Booking {} for {}>".format(self.userID, self.providerID)

    def __init__(self, userId, providerId, centreId, date, time, msg=None, notes=None):
        self.userId = userId
        self.centreId = centreId
        self.providerId = providerId
        self.date = str(date)
        self.time = time
        try:
            self.msg = msg
        except:
            pass
        self.notes = notes
        if isFlash:
            flash(("Booking successfully created.", "success"))

    def editNotes(self, notes):
        try:
            self.notes = notes
            if isFlash:
                flash(("Notes successfully added.", "success"))
        except:
            if isFlash:
                flash(("Failed to add notes (too many words).", "error"))
            pass
