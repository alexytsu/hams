$(".providerinput").hide();
$(document).ready(function () {
    $(".providerinput").hide();
    $(".timeholder").hide();

    $("#choosecentre").change(function () {
        $.getJSON("/_get_providers",
            {
                centre: $("#choosecentre option:selected").val(),
            },
            function (data) {
                $('#chooseprovider').empty();
                var arraylength = data.providers.length;
                for (var x in data.providers) {
                    if (data.providers[x].profession != "Specialist") {
                        $('#chooseprovider').append(
                            $('<option />').text(
                                data.providers[x].username + ":" + data.providers[x].profession
                            ).val(
                                data.providers[x].username
                            )
                        );
                    }
                }
            }
        );
        $(".providerinput").show(1000);
    });

    $("#chooseprovider").change(function () {
        $(".timeholder").show();
        for (let i = 0; i < 5; i++) {
            $("#timebuttons" + i).empty();
        }
        $.getJSON("/_get_timeslots",
            {
                centre: $("#choosecentre option:selected").val(),
                provider: $("#chooseprovider option:selected").val(),
            },
            function (data) {
                var avail = data.timeslot.availability;
                for (var i = 0; i < avail.length; i++) {
                    var day = Math.floor(i / 48);
                    var hourpart = i % 48 + 1;
                    var hour = Math.floor(hourpart / 2);
                    if (hour < 10) {
                        hour = "0" + hour;
                    }
                    var minute = hourpart % 2 == 1 ? "30" : "00";
                    var timestring = hour + ':' + minute;


                    if (avail.charAt(i) == 'a') {
                        b = $('<button/>', {
                            text: timestring,
                            value: i,
                            name: "chosentime",
                            class: "btn btn-success",
                        });
                        $("#timebuttons" + day).append(b);
                    } else {
                        b = $('<button/>', {
                            text: timestring,
                            disabled: true,
                            class: "btn btn-warning",
                        });
                        $("#timebuttons" + day).append(b);
                    }
                }
            }
        );
    });
});