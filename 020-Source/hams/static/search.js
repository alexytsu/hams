$(document).ready(function () {
    $("#filter_gp").change(function () {
        if ($("#filter_gp").is(":checked")) {
            $(".GP").show(300);
        } else {
            $(".GP").hide(300);
        }
    });
    $("#filter_physio").change(function () {
        if ($("#filter_physio").is(":checked")) {
            $(".Physiotherapist").show(300);
        } else {
            $(".Physiotherapist").hide(300);
        }
    });
    $("#filter_pharm").change(function () {
        if ($("#filter_pharm").is(":checked")) {
            $(".Pharmacist").show(300);
        } else {
            $(".Pharmacist").hide(300);
        }
    });
    $("#filter_path").change(function () {
        if ($("#filter_path").is(":checked")) {
            $(".Pathologist").show(300);
        } else {
            $(".Pathologist").hide(300);
        }
    });
    $("#filter_specialist").change(function () {
        if ($("#filter_path").is(":checked")) {
            $(".Specialist").show(300);
        } else {
            $(".Specialist").hide(300);
        }
    });
});