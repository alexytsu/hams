import os
import csv

from flask import flash

from hams import Database, app
from hams.rating.Rating import Rating
from hams.users.User import User
from hams.provider.Provider import Provider
from hams.centres.HealthcareCentre import Centre
from hams import isFlash


class RatingsManager():

    def import_csv(self):

        filepath = os.path.join(app.config['DATA'], "ratings.csv")
        with open(filepath) as ratingscsv:
            reader = csv.reader(ratingscsv, delimiter=',')
            for row in reader:
                username = row[0]
                rating = row[1]
                provider = row[2]
                centre = row[3]
                self.insert_rating(username, rating, provider, centre)

        flash(("Imported ratings", "success"))

    def insert_rating(self, username, rating, provider, centre):

        userId = User.query.filter_by(username=username).first()
        if userId is not None:
            userId = userId.username

        providerId = Provider.query.filter_by(username=provider).first()
        if providerId is not None:
            providerId = providerId.username

        centreId = Centre.query.filter_by(centrename=centre).first()
        if centreId is not None:
            centreId = centreId.centrename

        added = False

        if centreId is None:
            rating_old = Rating.query.filter_by(
                userId=userId, providerId=providerId).first()
            if(rating_old is not None):
                if(isFlash):
                    flash(("Your old rating of {} was deleted.".format(
                        rating_old.rating), "warning"))
                rating_old.rating = rating
                Database.session.commit()
                if(isFlash):
                    flash(("Adding your rating of {}".format(rating), "warning"))
                added = True

        if providerId is None:
            rating_old = Rating.query.filter_by(
                userId=userId, centreId=centreId).first()
            if(rating_old is not None):
                print(rating_old)
                if(isFlash):
                    flash(("Your old rating of {} was deleted.".format(
                        rating_old.rating), "warning"))
                rating_old.rating = rating
                Database.session.commit()
                if(isFlash):
                    flash(("Adding your rating of {}".format(rating), "error"))
                added = True

        if not added:
            if(isFlash):
                flash(("Adding your new rating of {}".format(rating), "success"))
            try:
                rating_entry = Rating(rating, userId, providerId, centreId)
                Database.session.add(rating_entry)
                Database.session.commit()
            except Exception:
                if(isFlash):
                    flash(("Unable to add", "error"))
                pass
