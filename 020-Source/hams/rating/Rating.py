from flask import flash
from hams import Database


class Rating(Database.Model):

    id = Database.Column(Database.Integer, primary_key=True)
    userId = Database.Column(Database.Integer)
    rating = Database.Column(Database.Integer)
    providerId = Database.Column(Database.Integer)
    centreId = Database.Column(Database.Integer)

    def __repr__(self):
        return "<Rating {}>".format(self.rating)

    def __init__(self, rating=None, userId=None, providerId=None, centreId=None):
        if (providerId is None):
            # if (len(Rating.query.filter_by(userId=self.userId, centreId=self.centreId)) >= 1):
            #     Rating.update().\
            #         where(Rating.userId == userId, Rating.centreId == centreId).\
            #         values(rating=rating)
            # else:
            self.userId = userId
            self.centreId = centreId
            self.rating = rating

        if (centreId is None):
            # if (len(Rating.query.filter_by(userId=self.userId, providerId=self.providerId)) >= 1):
            #     Rating.update().\
            #         where(Rating.userId == userId, Rating.providerId == providerId).\
            #         values(rating=rating)
            # else:
            self.userId = userId
            self.providerId = providerId
            self.rating = rating
