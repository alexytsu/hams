# Python libraries
from datetime import datetime
from datetime import timedelta
from math import floor

# Installed dependencies
from flask import (
    render_template, request, redirect, url_for, flash, jsonify
)
from flask_login import login_required, current_user, login_user, logout_user

# Custom dependencies
from hams import app, login_manager, Database
from hams.users.User import User
from hams.specialist.Specialist import specialist
from hams.specialist.SpecialistManager import SpecialistManager
from hams.users.UserManager import UserManager
from hams.centres.HealthcareCentre import Centre
from hams.centres.CentreManager import CentreManager
from hams.provider.ProviderManager import ProviderManager
from hams.provider.Provider import Provider
from hams.rating.RatingsManager import RatingsManager
from hams.provider.ProviderCentreTableManager import ProviderCentreTableManager
from hams.provider.ProviderCentreTable import ProviderCentreTable
from hams.patient.Patient import Patient
from hams.search.Search import SearchEngine
from hams.bookings.booking import Booking
from hams.bookings.BookingManager import BookingManager
from hams.prescriptions.Prescription import Prescription
from hams.bookings.BookingManager import BookingManager
from hams.prescriptions.PrescriptionManager import PrescriptionManager
from hams import isFlash

isFlash = 1

UserManager = UserManager()
CentreManager = CentreManager()
provider_manager = ProviderManager()
specialist_manager = SpecialistManager()
work_hour_manager = ProviderCentreTableManager()


@app.context_processor
def inject_current_user():
    isPatient = isProvider = False

    try:
        user = UserManager.get_user(current_user.username)
        if user is None:
            # isLoggedOut = True
            pass
        else:
            userType = UserManager.get_user_type(user.username)
            if(userType == "Patient"):
                isProvider = False
                isPatient = True
            elif(userType == "Provider"):
                isProvider = True
                isPatient = False
    except Exception:
        pass

    return dict(isPatient=isPatient, isProvider=isProvider, current_users=current_user)


@app.errorhandler(405)
def page_not_found(e):
    flash(("You have requested an invalid resource", "error"))
    return redirect(url_for('user_profile'))


@login_manager.user_loader
def load_user(user_id):
    return UserManager.get_user_by_ID(user_id)


@login_manager.unauthorized_handler
def unauthorized():
    flash(("You must log in to view that page", "warning"))
    return redirect(url_for('login'))


@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('user_profile'))
    if request.method == 'POST':
        username = request.form["username"]
        password = request.form["password"]
        registered_user = UserManager.get_user(username)
        if registered_user is None:
            flash(("Username or password wrong", "error"))
            return redirect(url_for('login'))
        if registered_user.validate(password):
            flash(("Logged in", "success"))
            login_user(registered_user)
            return redirect(url_for('user_profile'))
        else:
            flash(("Username or password was wrong", "error"))
            return redirect(url_for("login"))

    return render_template('auth/login.html')


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/login')


@app.route('/handle_rating', methods=['POST'])
@login_required
def handle_rating():
    rating = request.form['rating']
    centrename = None
    providername = None
    try:
        centrename = request.form['centrename']
    except KeyError:
        providername = request.form['providername']
    if centrename is not None:
        ratings_manager = RatingsManager()
        # ratings_manager.insert_rating_centre(current_user.id, rating, )
        ratings_manager.insert_rating(
            current_user.username, rating, None, centrename)
        return redirect(url_for('centre'))
    if providername is not None:
        ratings_manager = RatingsManager()
        ratings_manager.insert_rating(
            current_user.username, rating, providername, None)
        return redirect(url_for('provider_profile'))


@app.route('/profile', methods=["GET", "POST"])
@login_required
def user_profile():
    if(request.method == "POST"):
        name = request.form['patient']
        user = UserManager.get_user(name)
    else:
        user = current_user
    return render_template("profile/user_profile.html", user=user)


@app.route('/provider', methods=['get', 'post'])
@login_required
def provider_profile():
    specialists = []
    usertype = UserManager.get_user_type(current_user.username)
    user = UserManager.get_user(current_user.username)
    if usertype == "Patient":
        user = UserManager.Get_Patient(current_user.username)
        if(user.referal != ""):
            specialist = UserManager.get_Specialist(user.referal)
            specialists.append(specialist)

    if request.method == "POST":
        user_id = request.form['provider']
        print(user_id)
        providers = [UserManager.get_Provider(user_id)]
        providers[0].recalculate_rating()
    else:
        providers = list(UserManager.get_Provider())
        providers = [
            provider for provider in providers if provider.profession is not None
        ]
        providers = [
            provider for provider in providers if provider.profession != "specialist"]
        for provider in providers:
            provider.recalculate_rating()
    return render_template("profile/provider_profile.html", usertype=usertype, specialists=specialists, providers=providers)


@app.route('/centre', methods=['get', 'post'])
@login_required
def centre():
    centres = []
    if request.method == "POST":
        id = request.form['link']
        CentreList = CentreManager.get_all_centres()
        for loc in CentreList:
            if id == str(loc.code):
                loc.recalculate_rating()
                centres.append(loc)
    else:
        centres = CentreManager.get_all_centres()
        for centre in centres:
            centre.recalculate_rating()

    centre_providers = []
    for centre in centres:
        providers = []
        workers = UserManager.get_Providers_by_Centre(centre.centrename)
        for worker in workers:
            if(UserManager.get_Provider(worker.provider).profession != "Specialist"):
                providers.append(worker)
        centre_providers.append(providers)

    info = zip(centres, centre_providers)
    return render_template("profile/centre_profile.html", info=info)


@app.route('/history')
@login_required
def booking_hist():
    type = "provider"
    user = UserManager.get_Provider(current_user.username)
    if user.profession is None:
        type = "patient"
        bookings = BookingManager.getBookings(current_user.username)
    else:
        type = "provider"
        bookings = BookingManager.getProviderBookings(current_user.username)

    providerID = []
    CentreID = []
    for book in bookings:
        providerID.append(UserManager.get_Provider(book.providerId).providerNO)
        CentreID.append(CentreManager.get_centre_code(book.centreId))

    vals = list((zip(bookings, list(zip(providerID, CentreID)))))

    num = 1111
    return render_template("profile/booking_history.html", num=num, bookings=bookings, type=type, vals=vals)


@app.route('/search', methods=['get', 'post'])
@login_required
def search():

    srchengine = SearchEngine()
    provider_results = []
    centre_results = []

    if request.method == 'POST':
        searchterm = request.form['searchterm'].lower().strip()

        if not searchterm:
            flash(("No search term specified.", "warning"))
            return render_template("search/search.html", providers=provider_results, centres=centre_results, searchterm=None)

        provider_results, centre_results = srchengine.get_matches(searchterm)

    return render_template("search/search.html", providers=provider_results, centres=centre_results, searchterm=searchterm)


@app.route('/setup')
def setup():
    Database.drop_all()
    Database.create_all()

    #user_manager = UserManager()
    UserManager.import_csv()

    #centre_manager = CentreManager()
    CentreManager.import_csv()

    #provider_manager = ProviderManager()
    provider_manager.import_csv()

    #specialist_manager = SpecialistManager()
    specialist_manager.import_csv()

    #work_hour_manager = ProviderCentreTableManager()
    work_hour_manager.import_csv()

    return render_template("setup.html")


@app.route('/register', methods=["GET", "POST"])
def register():
    if request.method == 'POST':
        form = request.form
        user_manager = UserManager()
        if user_manager.register_user_from_form(form):
            return redirect(url_for("user_profile"))
        else:
            return redirect(url_for("register"))

    return render_template('auth/register.html')

# displays the booking form when a user wants to book
# with a particular provider


@app.route("/make_booking_given_provider", methods=["POST"])
@login_required
def make_booking_given_provider():
    providerusername = request.form["providerusername"]
    provider = UserManager.get_Provider(providerusername)
    centres = CentreManager.get_centres_of_provider(providerusername)
    specialist = UserManager.get_Specialist(providerusername)
    if specialist is not None:
        UserManager.Change_Patient_Referal(current_user.username, "")
    print(centres)
    return render_template("bookings/book_provider.html", provider=provider, centres=centres)


# displays the booking form
@app.route("/make_booking")
@login_required
def make_booking():
    all_centres = CentreManager.get_all_centres()
    return render_template("bookings/booking.html", centres=all_centres)


# an api endpoint to get all providers that work at a given centre
@app.route("/_get_providers")
def return_providers():
    centre_name = request.args.get('centre')
    providerList = []
    providers = list(UserManager.get_Providers_by_Centre(centre_name))

    for provider in providers:
        providername = provider.provider
        p = UserManager.get_Provider(providername)

        providerList.append(
            {
                "username": p.username,
                "profession": p.profession,
            }
        )

    return jsonify(providers=providerList)

# an api endpoint to get all timeslots for a particular provider
# at a particular centre that they work at


@app.route("/_get_timeslots")
def choose_timeslot():
    provider_name = request.args.get('provider')
    centre_name = request.args.get('centre')
    entries = UserManager.get_Providers_by_Centre(centre_name, provider_name)
    entries = list(entries)
    entry = entries[0]

    return jsonify(timeslot={"availability": entry.availability})

# an api endpoint to store a booking in the database


@app.route("/_store_booking", methods=["POST"])
def store_booking():
    provider_name = request.form["chosenprovider"]
    patient_name = current_user.username
    timeslot = request.form["chosentime"]
    centre_name = request.form["chosencentre"]
    message = request.form["info"]

    # days from now
    date = datetime.now()
    daysfromnow = int(int(timeslot) / 48)
    date = date + timedelta(days=daysfromnow)
    day = str(date.date())

    hour = int(timeslot) % 48 + 1
    minute = None
    if hour % 2 == 1:
        minute = "30"
    else:
        minute = "00"

    hour = floor(hour / 2)

    if hour < 10:
        hour = "0" + str(hour)
    else:
        hour = str(hour)

    timestring = hour + ":" + minute

    b = Booking(patient_name, provider_name,
                centre_name, day, timestring, message)
    BookingManager.addBooking(b)

    # now change the availability of the provider

    UserManager.Change_Provider_Avaliability(
        provider_name, timeslot, centre_name)

    return redirect(url_for("booking_hist"))


@app.route("/notes", methods=["POST"])
@login_required
def notes():
    specialists = UserManager.get_Specialist()
    specialists = [
        specialist for specialist in specialists if specialist.expertise is not None]
    if request.method == "POST":
        bookingID = request.form["bookingID"]
        booking = BookingManager.getBookingbyId(bookingID)
        patientId = booking.userId
        patient = UserManager.Get_Patient(patientId)
        view = patient.getViewNotes(current_user.username)
        edit = patient.getEditNotes(current_user.username)
        if not edit:
            edit = ""
        else:
            edit = edit + '\n' + '\n'
        edit = edit + "Timestamp: " + \
            str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        edit = edit + '\n' + "Doctor: " + current_user.username

    return render_template("history/notes.html", bookingID=bookingID, editNotes=edit, viewNotes=view, specialists=specialists)


@app.route("/storednotes", methods=["GET", "POST"])
@login_required
def store_notes():
    if request.method == "POST":
        notes = request.form["notes"]
        bookingID = request.form["bookingID"]
        if request.form["Refer"] != "0":
            specialistName = request.form["Refer"]
            Patientbooking = BookingManager.getBookingbyId(bookingID)
            patientName = (Patientbooking.userId)
            UserManager.Change_Patient_Referal(patientName, specialistName)
            Database.session.commit()

        booking = BookingManager.getBookingbyId(bookingID)
        booking.editNotes(notes)
        Database.session.commit()
    return redirect(url_for('booking_hist'))


@app.route("/prescriptions", methods=["GET", "POST"])
@login_required
def prescriptions():
    if request.method == "POST":
        bookingID = request.form["bookingID"]
        patient = BookingManager.getPatient(bookingID)
        prescriptions = PrescriptionManager.get_prescription(patient)
    return render_template("history/prescriptions.html", bookingID=bookingID, prescriptions=prescriptions)


@app.route("/storedprescriptions", methods=["GET", "POST"])
@login_required
def store_pres():
    if request.method == "POST":
        bookingID = request.form["bookingID"]
        medicine = request.form["medicine"]
        dosage = request.form["dosage"]
        frequency = request.form["frequency"]
        days = request.form["days"]
        info = request.form["info"]
        try:
            prescription = Prescription(
                bookingID, medicine, dosage, frequency, days, info)
            PrescriptionManager.store_prescription(prescription)
        except:
            pass
        return redirect(url_for('booking_hist'))
