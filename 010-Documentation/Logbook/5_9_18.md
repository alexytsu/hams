# Features Implemented of 5_9_18

## Features
- Dummy homepage
- Dummy searchpage that reads all healtcare centres form CSV and displays them
- Dummy user profile page
- Navbar
- flash() error handling in base template
- Bootstrap framework added
- User registration to a sql database, prevents duplicate users

## Git Log

* 49b9c13 - (HEAD -> alex, origin/master, origin/alex, origin/HEAD, master) implement user registration into an sql database (3 minutes ago) <Alexander Su>
* c99e9d1 - added a register feature for users (5 minutes ago) <Alexander Su>
* b2fecfe - base template includes css stylesheet (60 minutes ago) <Alexander Su>
* eeede56 - homepage implemented (60 minutes ago) <Alexander Su>
* 6497aac - move data files (61 minutes ago) <Alexander Su>
* 7b13cd4 - base website created with navbar and pagecontent block (3 hours ago) <Alexander Su>
* 586eda8 - create setup files and templates (4 hours ago) <Alexander Su>
* 13ae79f - create new template for hello world based of flask doc (11 hours ago) <Alexander Su>
* 8e82e20 - example database file (2 days ago) <Alexander Su>
* d454b03 - created init (2 days ago) <Alexander Su>
* 48f9791 - added a nice gitignore (2 days ago) <Alexander Su>
*   7a54df6 - Merge branch 'master' of https://github.com/cs1531/group-letters_numbers-underscores (2 days ago) <Alexander Su>
|\
| * d9b976f - Create 2_9_18.md (2 days ago) <Alexander Su>
* | 5ea7efa - added meeting notes (2 days ago) <Alexander Su>
|/
* d1a203e - added takeaways to meeting (5 days ago) <Alexander Su>
* 730bbf3 - had a meeting (5 days ago) <Alexander Su>
*   813d2b5 - Merge branch 'alex' (5 days ago) <Alexander Su>
|\
| * adab970 - added documentation (5 days ago) <Alexander Su>
* | 6faaa1e - created file to hold the deliverables for week6 (7 days ago) <Alexander Su>
|/
* dcb0de6 - created meeting minutes file (7 days ago) <Alexander Su>
* 55ee027 - organise files for project (7 days ago) <Alexander Su>
* a823854 - added other csv files (3 weeks ago) <Anna>
* 31cfcf6 - (tag: ass-release) Added CSV file (3 weeks ago) <Anna>